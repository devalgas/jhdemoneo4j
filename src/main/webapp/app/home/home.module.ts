import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JhDemoNeo4JSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';

@NgModule({
  imports: [JhDemoNeo4JSharedModule, RouterModule.forChild([HOME_ROUTE])],
  declarations: [HomeComponent],
})
export class JhDemoNeo4JHomeModule {}
