import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhDemoNeo4JSharedModule } from 'app/shared/shared.module';

import { LogsComponent } from './logs.component';

import { logsRoute } from './logs.route';

@NgModule({
  imports: [JhDemoNeo4JSharedModule, RouterModule.forChild([logsRoute])],
  declarations: [LogsComponent],
})
export class LogsModule {}
