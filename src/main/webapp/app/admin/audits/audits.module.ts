import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhDemoNeo4JSharedModule } from 'app/shared/shared.module';

import { AuditsComponent } from './audits.component';

import { auditsRoute } from './audits.route';

@NgModule({
  imports: [JhDemoNeo4JSharedModule, RouterModule.forChild([auditsRoute])],
  declarations: [AuditsComponent],
})
export class AuditsModule {}
