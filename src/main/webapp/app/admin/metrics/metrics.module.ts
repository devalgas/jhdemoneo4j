import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhDemoNeo4JSharedModule } from 'app/shared/shared.module';

import { MetricsComponent } from './metrics.component';

import { metricsRoute } from './metrics.route';

@NgModule({
  imports: [JhDemoNeo4JSharedModule, RouterModule.forChild([metricsRoute])],
  declarations: [MetricsComponent],
})
export class MetricsModule {}
