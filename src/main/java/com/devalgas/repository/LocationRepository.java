package com.devalgas.repository;

import com.devalgas.domain.Location;

import org.neo4j.springframework.data.repository.Neo4jRepository;
import org.neo4j.springframework.data.repository.query.Query;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 * Spring Data Neo4j repository for the Location entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LocationRepository extends Neo4jRepository<Location, String> {
}
