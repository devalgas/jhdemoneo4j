package com.devalgas.repository;

import com.devalgas.domain.Job;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.neo4j.springframework.data.repository.Neo4jRepository;
import org.neo4j.springframework.data.repository.query.Query;
import java.util.List;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data Neo4j repository for the Job entity.
 */
@Repository
public interface JobRepository extends Neo4jRepository<Job, String> {

    @Query("MATCH (n:Job)<-[]-(m) RETURN n,m")
    Page<Job> findAllWithEagerRelationships(Pageable pageable);

    @Query("MATCH (e:Job {id: $id}) RETURN e")
    Optional<Job> findOneWithEagerRelationships(String id);
}
