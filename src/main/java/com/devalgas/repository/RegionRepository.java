package com.devalgas.repository;

import com.devalgas.domain.Region;

import org.neo4j.springframework.data.repository.Neo4jRepository;
import org.neo4j.springframework.data.repository.query.Query;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 * Spring Data Neo4j repository for the Region entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegionRepository extends Neo4jRepository<Region, String> {
}
