package com.devalgas.repository;

import com.devalgas.domain.Country;

import org.neo4j.springframework.data.repository.Neo4jRepository;
import org.neo4j.springframework.data.repository.query.Query;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 * Spring Data Neo4j repository for the Country entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CountryRepository extends Neo4jRepository<Country, String> {
}
