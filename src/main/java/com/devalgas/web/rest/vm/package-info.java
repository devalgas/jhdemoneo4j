/**
 * View Models used by Spring MVC REST controllers.
 */
package com.devalgas.web.rest.vm;
