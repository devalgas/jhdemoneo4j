/**
 * Neo4j specific configuration and required migrations.
 */
package com.devalgas.config.neo4j;
